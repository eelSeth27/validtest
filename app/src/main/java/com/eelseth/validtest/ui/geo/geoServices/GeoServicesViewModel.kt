package com.eelseth.validtest.ui.geo.geoServices

import android.arch.lifecycle.ViewModel
import com.eelseth.validtest.model.Artist
import com.eelseth.validtest.webServices.api.GeoServicesApi
import com.eelseth.validtest.webServices.getRetrofit
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject

class GeoServicesViewModel() : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    val artistList = BehaviorSubject.create<List<Artist>>()

    private val geoService by lazy {
        getRetrofit().create(GeoServicesApi::class.java)
    }

    fun getTopArtist() {
        compositeDisposable.add(
                geoService.getGeoTopArtists()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            artistList.onNext(it.topartists.artist)

                        }, Throwable::printStackTrace))


    }


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }



}
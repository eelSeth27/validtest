package com.eelseth.validtest.ui.geo

import android.os.Bundle
import android.widget.FrameLayout
import butterknife.BindView
import butterknife.ButterKnife
import com.eelseth.validtest.R
import com.eelseth.validtest.base.BaseActivity
import com.eelseth.validtest.ui.geo.geoServices.GeoServicesFragment
import dagger.android.AndroidInjection

class GeoInfoActivity : BaseActivity() {

    @BindView(R.id.fl_container)
    lateinit var container: FrameLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_geo_info)
        //AndroidInjection.inject(this)
        ButterKnife.bind(this)

        if(savedInstanceState == null){
            loadFragment(GeoServicesFragment())
        }
        init()
    }

    private fun init() {

    }
}

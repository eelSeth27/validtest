package com.eelseth.validtest.ui.geo.geoServices

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eelseth.validtest.R
import com.eelseth.validtest.base.BaseFragment
import com.eelseth.validtest.model.Artist
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class GeoServicesFragment : BaseFragment() {

    private val compositeDisposable = CompositeDisposable()

    //    @Inject
//    lateinit var viewModelFactory: ViewModelProvider.Factory
//
    private lateinit var viewModel: GeoServicesViewModel

//    @BindView(R.id.rv_artists)
//    lateinit var recyclerView: RecyclerView

    private val recyclerView by lazy { view?.findViewById<RecyclerView>(R.id.rv_artists) }

    override fun onCreate(savedInstanceState: Bundle?) {
        //AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(GeoServicesViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_geo_services, container, false)
        //ButterKnife.bind(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        recyclerView?.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        }




        subscribeToViewModel()
        viewModel.getTopArtist()
    }

    fun subscribeToViewModel() {
        compositeDisposable.add(viewModel.artistList.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    loadArtists(it)

                }, Throwable::printStackTrace))
    }

    fun loadArtists(artists: List<Artist>) {
        recyclerView?.adapter =  ArtistAdapter(artists)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }


}
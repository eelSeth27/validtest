package com.eelseth.validtest.ui.geo.geoServices

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eelseth.validtest.R
import com.eelseth.validtest.model.Artist
import kotlinx.android.synthetic.main.artist_item.view.*

class ArtistAdapter(private val artistList: List<Artist>) :
        RecyclerView.Adapter<ArtistAdapter.ArtistViewHolder>() {

    class ArtistViewHolder(
            val view: View
    ) : RecyclerView.ViewHolder(view){
        val tvName = view.tv_name
        val tvListeners = view.tv_listeners
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ArtistAdapter.ArtistViewHolder {

        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.artist_item, parent, false)



        return ArtistViewHolder(view)
    }

    override fun onBindViewHolder(holder: ArtistViewHolder, position: Int) {
        holder.tvName.text = artistList[position].name
        holder.tvListeners.text = artistList[position].listeners.toString()
    }

    override fun getItemCount() = artistList.size
}
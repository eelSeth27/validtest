package com.eelseth.validtest

import android.support.v7.app.AppCompatDelegate
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.core.CrashlyticsCore
import com.eelseth.validtest.di.components.AppComponent
import com.eelseth.validtest.di.components.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import io.fabric.sdk.android.Fabric

class GeoApp : DaggerApplication() {

    companion object {
        @JvmStatic
        lateinit var graph: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        Fabric.with(this, Crashlytics.Builder().core(CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build()).build())
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        graph = DaggerAppComponent.builder().application(this).build()
        graph.inject(this)
        return graph
    }

}
package com.eelseth.validtest.di.components

import android.app.Application
import com.eelseth.validtest.GeoApp
import com.eelseth.validtest.base.di.DaggerViewModelInjectionModule
import com.eelseth.validtest.di.modules.ApiModule
import com.eelseth.validtest.di.modules.AppModule
import com.eelseth.validtest.di.modules.BuildersModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(
        modules = [
            AppModule::class,
            ApiModule::class,
            BuildersModule::class,
            AndroidSupportInjectionModule::class,
            DaggerViewModelInjectionModule::class
        ]
)

interface AppComponent : AndroidInjector<GeoApp> {

    fun inject(application: Application)


    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(app: Application): Builder

        fun build(): AppComponent
    }
}
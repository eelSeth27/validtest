package com.eelseth.validtest.di.modules

import android.arch.lifecycle.ViewModel
import com.eelseth.validtest.base.di.ViewModelKey
import com.eelseth.validtest.di.scopes.ActivityScope
import com.eelseth.validtest.di.scopes.FragmentScope
import com.eelseth.validtest.ui.geo.GeoInfoActivity
import com.eelseth.validtest.ui.geo.geoServices.GeoServicesFragment
import com.eelseth.validtest.ui.geo.geoServices.GeoServicesViewModel
import com.eelseth.validtest.webServices.api.GeoServicesApi
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module
abstract class BuildersModule {

    @ActivityScope
    @ContributesAndroidInjector()
    abstract fun contributeGeoInfoActivity(): GeoInfoActivity

    @FragmentScope
    @ContributesAndroidInjector()
    abstract fun contributeGeoServicesFragment(): GeoServicesFragment

    @Binds
    @IntoMap
    @ViewModelKey(GeoServicesViewModel::class)
    abstract fun bindGeoServicesViewModel(geoServicesViewModel: GeoServicesViewModel): ViewModel

}


@Module
class ApiModule {

    @Provides
    fun provideUserApi(retrofit: Retrofit): GeoServicesApi = retrofit.create(GeoServicesApi::class.java)

}
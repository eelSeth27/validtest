package com.eelseth.validtest.webServices.responses

import com.eelseth.validtest.model.Artist
import com.eelseth.validtest.model.Attr
import com.google.gson.annotations.SerializedName

data class GeoTopArtistsResponse(val topartists: TopArtists)


data class TopArtists(
        val artist: List<Artist> = emptyList(),
        @SerializedName("@attr") val attrs: Attr)
package com.eelseth.validtest.webServices.api

import com.eelseth.validtest.webServices.responses.GeoTopArtistsResponse
import io.reactivex.Single
import retrofit2.http.GET

const val API_KEY = "829751643419a7128b7ada50de590067"
const val KEY = "key"
const val URL_TOP_ARTISTS = "http://ws.audioscrobbler.com/2.0/?method=geo.gettopartists&country=spain&api_key=829751643419a7128b7ada50de590067&format=json"
const val URL_TOP_TRACKS = "?method=geo.gettoptracks&country=spain&api_key={$API_KEY}&format=json"

interface GeoServicesApi {

    @GET(URL_TOP_ARTISTS)
    fun getGeoTopArtists(): Single<GeoTopArtistsResponse>

}
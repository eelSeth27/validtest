package com.eelseth.validtest.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.eelseth.validtest.R

abstract class BaseActivity : AppCompatActivity() {

    fun loadFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.fl_container, fragment)
                .commit()
    }
}
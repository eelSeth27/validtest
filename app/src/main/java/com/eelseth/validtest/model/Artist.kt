package com.eelseth.validtest.model

data class Artist(
        val name: String,
        val listeners: Int,
        val url: String,
        val mbid: String)
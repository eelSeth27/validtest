package com.eelseth.validtest.model

data class Attr(
        val country: String,
        val page: Int,
        val perPage: Int,
        val mbid: String)